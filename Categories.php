<?php session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<script type="text/javascript" src="js/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<title>UKDW Showcase home</title>
</head>
<body>
	<header>
		<nav class="nav-main">
			<?php if(isset($_SESSION["nama"])):?>
			<ul>
				<li >
					<a href="profile.php" class="nav-item sign">Profile , <?php echo $_SESSION["nama"]; ?></a>
				</li>
				<li>
					<a href="logout.php" class="nav-item sign">Sign Out</a>
				</li>
			</ul>
			<?php
			else: ?>
			<ul>
				<li >
					<a href="signup.html" class="nav-item sign">Sign up</a>
				</li>
				<li>
					<a href="login.html" class="nav-item sign">Sign in</a>
				</li>
			</ul>
		<?php  endif;?>
		</nav>
		<img src="images/FTI_showcase_header.png" alt="tampilan logo header">
	</header>
	<div id="navline">
		<nav class="nav-main" id="kirii">
			<ul>
				<li>
					<a href="index.php" class="nav-item">Home</a>
				</li>
				<li>
					<a href="categories.php" class="nav-item">Categories</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="category.php"> Akademik</a>
									</li>
									<li>
										<a href="category.php"> Non-Akademik</a>
									</li>
									<li>
										<a href="category.php"> Reguler</a>
									</li>
								</ul>
							</div>
						</div>
				</li>
				<li>
					<a href="reward.php" class="nav-item">Award</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="national.php"> National</a>
									</li>
									<li>
										<a href="reward.php"> International</a>
									</li>
									<li>
										<a href="reward.php"> Daerah </a>
									</li>
								</ul>
							</div>
						</div>
				</li>								
			</ul>
		</nav>
		<nav class="nav-main" id="kanann">				
			<ul>
				<?php if(isset($_SESSION["nama"])): ?>
				<li>
					<a href="add.php" class="nav-item">
						Add
					</a>
				</li>
				<?php endif;?>
				<li> 
					<div>
						<form method="get" action="search.php">					
							<input type="text" name="search" id="search-input" placeholder="Search">
							<input src="images/search-icon.png" onclick="validasiSearch()" name="submit" type="image" id="search-input-img" alt="logo search"/>
						</form>
					</div>
				</li>
	
			</ul>
		</nav>
	</div>
	
	<table> 
		<tr>
			<td id="datakiri">
				<table id="cat1">
					<tr>
						<td>
							<h1>Categories</h1>
							<p>Banyak kegiatan yang kami lakukan  dari tahun ke tahun dan itu dapat dilihat dari banyaknya rangkaian acara yang telah terlaksana. Kegiatan maupun acara tersebut kami bagi atas 3 bagian:</p>
						</td>
					</tr>
					<tr>
						<td>
							<a href="akademik.php">
								<h4>Akademik</h4>
							</a>
							<img src="http://ti.ukdw.ac.id/img/kegiatan/27ec95da_IMG_7337.JPG" alt="kegiatan akademik">
						</td>
					</tr>
					<tr>
						<td>
							<a href="nonakademik.php">
								<h4>Non-Akademik</h4>
							</a>
							<img src="https://simix.files.wordpress.com/2012/09/250286_3483956349231_725385865_n-1.jpg" alt="kegiatan non akademik">
						</td>
					</tr>
					<tr>
						<td>
							<a href="other.php">
								<h4>Reguler</h4>
							</a>
							<img src="http://ti.ukdw.ac.id/img/kegiatan/4c8750cd_foto_lomba_desain_logo_bem.jpg" alt="kegiatan reguler">
						</td>
					</tr>
				</table>
				
			
			</td>
			<td id="datakanan"> 
				
				<h3>Video</h3> 
				<iframe height="300" src="https://www.youtube.com/embed/MqPJU8FU190">
				</iframe>
				<h3>Tips Creative</h3>
				<ol>
					<li>Imagination</li>
					<li>Do Something New</li>
					<li>Don't limit your self</li>
				</ol>
			</td>
		</tr>
	</table>
			
	<footer>
		<table>
			<tr>
				<td>
					<img src="images/FTI_showcase_footer.png" alt="tampilan logo footer"> <br><br>
					<a href="contectus.php">Contact Us</a><br>
					<a href="aboutus.php">About Us</a>
				</td>
					
				<td>
					<p><b>UNIVERSITAS KRISTEN DUTA WACANA</b><br>Jl. Dr. Wahidin Sudiro Husodo No. 5 - 25<br>Yogyakarta 55224<br>Telp. 0274 - 563929 Fax. 0274 - 513235<br>Email: humas@staff.ukdw.ac.id<br></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" >
					<h4>&copy; Mahasiswa TI 2016</h4>
				</td>
			</tr>
		</table>
	</footer>	
</body>
</html>