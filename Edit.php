<?php session_start(); 
if(isset($_SESSION["nama"]))
{
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<script type="text/javascript" src="js/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<title>UKDW Showcase home</title>
</head>
<body>
	<header>
		<nav class="nav-main">
			<?php if(isset($_SESSION["nama"])):?>
			<ul>
				<li >
					<a href="profile.php" class="nav-item sign">Profile  , <?php echo $_SESSION["nama"]; ?></a>
				</li>
				<li>
					<a href="logout.php" class="nav-item sign">Sign Out</a>
				</li>
			</ul>
			<?php
			else: ?>
			<ul>
				<li >
					<a href="signup.html" class="nav-item sign">Sign up</a>
				</li>
				<li>
					<a href="login.html" class="nav-item sign">Sign in</a>
				</li>
			</ul>
		<?php  endif;?>
		</nav>
		<img src="images/FTI_showcase_header.png" alt="tampilan logo header">
	</header>
	<div id="navline">
		<nav class="nav-main" id="kirii">
			<ul>
				<li>
					<a href="index.php" class="nav-item">Home</a>
				</li>
				<li>
					<a href="categories.php" class="nav-item">Categories</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="category.php"> Akademik</a>
									</li>
									<li>
										<a href="category.php"> Non-Akademik</a>
									</li>
									<li>
										<a href="category.php"> Reguler</a>
									</li>
								</ul>
							</div>
						</div>
				</li>
				<li>
					<a href="reward.php" class="nav-item">Award</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="national.php"> National</a>
									</li>
									<li>
										<a href="reward.php"> International</a>
									</li>
									<li>
										<a href="reward.php"> Daerah </a>
									</li>
								</ul>
							</div>
						</div>
				</li>								
			</ul>
		</nav>
		<nav class="nav-main" id="kanann">				
			<ul>
				<?php if(isset($_SESSION["nama"])): ?>
				<li>
					<a href="add.php" class="nav-item">
						Add
					</a>
				</li>
				<?php endif;?>
				<li> 
					<div>
						<form method="get" action="search.php">					
							<input type="text" name="search" id="search-input" placeholder="Search">
							<input src="images/search-icon.png" onclick="validasiSearch()" name="submit" type="image" id="search-input-img" alt="logo search"/>
						</form>
					</div>
				</li>

							
			</ul>
		</nav>
	</div>
	<table id="datasingle"> 
				<tr>
					<td>
						<h3>Edit Post</h3>
						
						<?php 
							require_once("database.php");

							$id=$_GET["id"];

							$row= get_user_post($id);

							if($row["Username"]!=$_SESSION["nama"])
							{
								echo $row["Username"];
								header("Location: profile.php");
							}

						?>
						
						<form enctype="multipart/form-data" action="editpost.php" method="post">
							Judul:<input type="text" name="judul" id="judul" style="width:500px" <?php echo "value='$row[Judul]' "; ?> ></input>
							<br>
							Teks:
							<br>

							<textarea cols="100" rows="7" name="posting" id="posting"><?php echo $row["Posting"]; ?></textarea>
							<br>	

							<img src="<?php echo $row["Image"]; ?>">

							<br>
							 Change Image:
							 <input type="file" name="image" id="image">
							 Kategori : 
							 <select name="kategori" id="kategori">
						         <option  value="-1">-- Pilih --</option>
								     <?php
								     require_once("database.php");
								     $koneksi = connect_database();

									 $sql=mysqli_query($koneksi,"select * from kategori");
									 while($rows=mysqli_fetch_assoc($sql)){
									 
										 if($row["KategoriID"]==$rows["Kode"])
										 {
										 	echo "<option value='$rows[Kode]' selected>$rows[Kategori]</option>";
										 }
										 else
										 {
										 	echo "<option value='$rows[Kode]'>$rows[Kategori]</option>";
										 }
									 
									 }
									 ?>
						     </select>
						     <input type="text" name="postid" id="postid" value="<?php echo $row["PostID"] ?>" hidden></input>
							 <input type="submit" name="submit" value="Upload"></input>
					</form>
					</td>
				</tr>
	</table>
			
	<footer>
		<table>
			<tr>
				<td>
					<img src="images/FTI_showcase_footer.png" alt="tampilan logo footer"> <br><br>
					<a href="contectus.php">Contact Us</a><br>
					<a href="aboutus.php">About Us</a>
				</td>
					
				<td>
					<p><b>UNIVERSITAS KRISTEN DUTA WACANA</b><br>Jl. Dr. Wahidin Sudiro Husodo No. 5 - 25<br>Yogyakarta 55224<br>Telp. 0274 - 563929 Fax. 0274 - 513235<br>Email: humas@staff.ukdw.ac.id<br></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" >
					<h4>&copy; Mahasiswa TI 2016</h4>
				</td>
			</tr>
		</table>
	</footer>	
</body>
</html>
<?php } 
else header("Location: index.html");
?>