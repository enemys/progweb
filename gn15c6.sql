-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2016 at 02:06 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gn15c6`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `Kode` int(11) NOT NULL,
  `Kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`Kode`, `Kategori`) VALUES
(1, 'Akademik'),
(2, 'Non-Akademik'),
(3, 'Award International'),
(4, 'Award National'),
(5, 'Award Daerah'),
(6, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `Judul` varchar(100) NOT NULL,
  `Posting` varchar(2000) NOT NULL,
  `Username` varchar(32) NOT NULL,
  `Tanggal` date NOT NULL,
  `PostID` int(12) NOT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `KategoriID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`Judul`, `Posting`, `Username`, `Tanggal`, `PostID`, `Image`, `KategoriID`) VALUES
('UKDW Showcase', 'Website ini dibuat untuk mewadahi mahasiswa FTI UKDW dalam menampilkan hasil karya mereka kepada khalayak umum, baik seluruh mahasiswa UKDW maupun bukan mahasiswa UKDW. Seluruh mahasiswa FTI UKDW dapat ikut berpartisipasi dalam website ini. Mereka dapat membuat akun khusus dengan email @ti.ukdw.ac.id agar dapat menjadi narasumber pada wbsite ini. Tentunya untuk mengawasi isi content yang ada, akan ada satu user yang bertindak sebagai admin, dimana tugas admin adalah memfilter content yang di upload oleh narasumber lalu memberikan persetujuan apakah content dari narasumber layak untuk dipublikasikan maupun tidak. Jika tidak admin berhak untuk tidak mempublikasikan content dari narasumber dan dapat memberikan warning.\\r\\n				Harapan dengan adanya website ini seluruh khalayak umum dapat melihat secara keseluruhan kegiatan-kegiatan yang dilakukan oleh mahasiswa FTI UKDW secara umum, baik dari segi prestasi maupun kegiatan reguler yang dilakukan dengan berita-berita terbaru.\\r\\n', 'Admin', '2016-04-24', 1, 'post/Admin26-04-2016-01-36-05.jpg', 6),
('Visi & Misi HMTI UKDW', 'Visi:\\r\\nMenjadikan Teknik Informatika yang Aktif dan Produktif dalam bidang Akademik dan non-Akademik\\r\\nMisi: \\r\\nMenyelenggarakan kegiatan yang mengembangkan mahasiswa dalam bidang akademik maupun non-akademik \\r\\nMemacu menekankan rasa kepedulian mahasiswa Teknik Informatika terhadap lingkungan kampus \\r\\nMemacu dan mengembangkan potensi mahasiswa untuk berorganisasi, berprestasi dan memajukan Teknik Informatika UKDW \\r\\nMenjadi wadah Aspirasi bagi civitas Teknik Informatika UKDW.', 'Admin', '2016-04-25', 2, 'post/Admin25-04-2016-02-13-36.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Username` varchar(32) NOT NULL,
  `Email` varchar(32) NOT NULL,
  `Fullname` varchar(50) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `TanggalLahir` date NOT NULL,
  `Profilpicture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Username`, `Email`, `Fullname`, `Password`, `TanggalLahir`, `Profilpicture`) VALUES
('Admin', 'Admin@admin.com', 'Admin', 'Admin', '0000-00-00', 'profilpicture/Admin.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`PostID`),
  ADD KEY `Username` (`Username`),
  ADD KEY `KategoriID` (`KategoriID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Email`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `user` (`Username`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`KategoriID`) REFERENCES `kategori` (`Kode`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
