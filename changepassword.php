<?php
	session_start();
	if(isset($_SESSION["nama"]) )
	{
	$var = $_SESSION["nama"];
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<script type="text/javascript" src="js/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<title>UKDW Showcase login</title>
</head>
<body>
	<header>
		<nav class="nav-main">
						<ul>
						
								<li>
									<a href="index.php" class="nav-item">Home</a>
								</li>
								<li >
									<a href="profile.php" class="nav-item sign">Profile , <?php echo $_SESSION["nama"]; ?></a>
								</li>
								
						</ul>
		</nav>
		<img src="images/FTI_showcase_header.png" alt="tampilan logo header">
	</header>
	<hr>
		<div id="login">
					<h4 style="padding-top: 20px; padding-left: 60px;">Change Password</h4>
					<hr style="border-width: 2px">	
				<br>

				<form method="post" action="change_password_process.php">
					<p style="padding-left:80px; ">
						Old Password:<br>
						<input type="text" name="oPassword"  id="oPassword" placeholder="Old Password" style="margin-left: 30px; width:50%;" >
					</p>
					<p style="padding-left: 80px">
						new Password:<br>
						<input type="password" name="nPassword" id="password" placeholder="New Password" style="margin-left: 30px; width:50%;" onblur="validasiPassword()">
					</p>

					<p style="padding-left: 80px">
						Retype new Password:<br>
						<input type="password" name="rPassword" id="password" placeholder="Retype new Password" style="margin-left: 30px; width:50%;">
					</p>
					<br>
					<p>
						<button style="margin-left: 300px;"id="signinBtn"  class="nav-item" type="submit"> Sent</button>
						
					</p>	

					<?php if(isset($_GET["status"]))
					{
						echo 	"<span style='margin-left: 100px;'>$_GET[status]</span>";
					}
						?>				
				</form>
			</div>

	<footer>
		<table>
			<tr>
				<td>
					<img src="images/FTI_showcase_footer.png" alt="tampilan logo footer"> <br><br>
					<a href="contectus.php">Contact Us</a><br>
					<a href="aboutus.php">About Us</a>
				</td>
					
				<td>
					<p><b>UNIVERSITAS KRISTEN DUTA WACANA</b><br>Jl. Dr. Wahidin Sudiro Husodo No. 5 - 25<br>Yogyakarta 55224<br>Telp. 0274 - 563929 Fax. 0274 - 513235<br>Email: humas@staff.ukdw.ac.id<br></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" >
					<h4>&copy; Mahasiswa TI 2016</h4>
				</td>
			</tr>
		</table>
	</footer>	
	
</body>
</html>
<?php } 
else header("Location: index.html");
?>