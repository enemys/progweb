<?php session_start(); 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<script type="text/javascript" src="js/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<title>UKDW Showcase home</title>
</head>
<body>
	<header>
		<nav class="nav-main">
			<?php if(isset($_SESSION["nama"])):?>
			<ul>
				<li >
					<a href="profile.php" class="nav-item sign">Profile , <?php echo $_SESSION["nama"]; ?></a>
				</li>
				<li>
					<a href="logout.php" class="nav-item sign">Sign Out</a>
				</li>
			</ul>
			<?php
			else: ?>
			<ul>
				<li >
					<a href="signup.html" class="nav-item sign">Sign up</a>
				</li>
				<li>
					<a href="login.html" class="nav-item sign">Sign in</a>
				</li>
			</ul>
		<?php  endif;?>
		</nav>
		<img src="images/FTI_showcase_header.png" alt="tampilan logo header">
	</header>
	<div id="navline">
		<nav class="nav-main" id="kirii">
			<ul>
				<li>
					<a href="index.php" class="nav-item">Home</a>
				</li>
				<li>
					<a href="categories.php" class="nav-item">Categories</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="category.php"> Akademik</a>
									</li>
									<li>
										<a href="category.php"> Non-Akademik</a>
									</li>
									<li>
										<a href="category.php"> Reguler</a>
									</li>
								</ul>
							</div>
						</div>
				</li>
				<li>
					<a href="reward.php" class="nav-item">Award</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="national.php"> National</a>
									</li>
									<li>
										<a href="reward.php"> International</a>
									</li>
									<li>
										<a href="reward.php"> Daerah </a>
									</li>
								</ul>
							</div>
						</div>
				</li>								
			</ul>
		</nav>
		<nav class="nav-main" id="kanann">				
			<ul>
				<?php if(isset($_SESSION["nama"])): ?>
				<li>
					<a href="add.php" class="nav-item">
						Add
					</a>
				</li>

				<?php endif;?>
				<li> 
					<div>
						<form method="get" action="search.php">					
							<input type="text" name="search" id="search-input" placeholder="Search">
							<input src="images/search-icon.png" onclick="validasiSearch()" name="submit" type="image" id="search-input-img" alt="logo search"/>
						</form>
					</div>	
			</ul>
		</nav>
	</div>

	<table> 
		<?php 
			require_once("database.php");

			$koneksi= connect_database();
			$page=0;

			if(!isset($_GET['page']))
			{
				$result=mysqli_query($koneksi,"SELECT * from post WHERE Status = 2 and KategoriID = 5 ORDER BY PostID DESC Limit 0,5");
				$r=mysqli_query($koneksi,"SELECT * from post WHERE Status = 2 and KategoriID = 5");
			}
			else
			{
				$page=$_GET["page"]; 
				$result=mysqli_query($koneksi,"SELECT * from post WHERE Status = 2 and KategoriID = 5 ORDER BY PostID DESC Limit $page,5");
				$r=mysqli_query($koneksi,"SELECT * from post WHERE Status = 2 and KategoriID = 5");
			}
			
			$count=0;
			$jmlh=mysqli_num_rows($r);
						
			while($row=mysqli_fetch_assoc($result)){
				$post = $row["Posting"];
		?>
			<tr style="margin-top: 5px">
				<hr style="border: 1px solid white">

				<td id="datakiri" class="gray">
					<h1><?php echo  $row['Judul']?></h1>
					<hr style="border: 1px solid white; border-width: 3px">

					<?php if($row["Image"]!= ""):?>
						<img src=<?php  echo "$row[Image]"?> style="width: 400px;  text-align: center;">
					<?php endif; ?>	

					<p>
						<?php echo nl2br_v2('\r\n'.$post);?>
					</p>

					By:
					<?php 
						if(isset($_SESSION["nama"])) 
						{
							echo "<a href='profile.php?profile=$row[Username]'>$row[Username]</a>";
						}
						else
						{
							echo $row["Username"];
						} ?>
						<?php if( isset($_SESSION["nama"]))
								{
									if($row["Username"] == $_SESSION["nama"]):?>
										<span style="float: right;"><span><?php echo $row["Vote"];?> </span><span>Vote</span></span>
						<?php 		else:?>
										<button onclick="vote( <?php echo $row['PostID'].",".$count;?> )" style="float: right;"><span name="voteval" id="voteval" class="voteval<?php echo $count;?>"><?php echo $row["Vote"];?> </span><span>&nbsp</span>Vote</button>
						<?php 		endif;
								}?>
					<!--<a href="vote.php?postid=<?php echo $row['PostID'];?>&vote=<?php echo $row['PostID'];?>".$row>Vote</a>-->
				</td>
			<?php 
			if($count==0)
			{
			?>
				<td id="datakanan"> 
					<div>				
					
					<h3>Video</h3> 
					<iframe width="400" height="300" src="https://www.youtube.com/embed/MqPJU8FU190">
					</iframe>
					<h3>Tips Creative</h3>
					<ol>
						<li>Imagination</li>
						<li>Do Something New</li>
						<li>Don't limit your self</li>
					</ol>
					</div>
				</td>
			</tr>
			
			<?php 
				}
				$count+=1;
			}
		?>
				
	</table>
	<div style="text-align: center">
		<?php
			if($jmlh>5)
			{
				if($page>0)
				{
					echo "<a href='index.php?page=".($page-5)."	 '>back</a>";
					echo "&nbsp;";
				}
				if(($page+5)<$jmlh)
				{
					echo "<a href='index.php?page=".($page+5)." '>next</a>";
				}
			} 	
		?>
		<br>
	</div>

			
	<footer>
		<table>
			<tr>
				<td>
					<img src="images/FTI_showcase_footer.png" alt="tampilan logo footer"> <br><br>
					<a href="contectus.php">Contact Us</a><br>
					<a href="aboutus.php">About Us</a>
				</td>
					
				<td>
					<p><b>UNIVERSITAS KRISTEN DUTA WACANA</b><br>Jl. Dr. Wahidin Sudiro Husodo No. 5 - 25<br>Yogyakarta 55224<br>Telp. 0274 - 563929 Fax. 0274 - 513235<br>Email: humas@staff.ukdw.ac.id<br></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" >
					<h4>&copy; Mahasiswa TI 2016</h4>
				</td>
			</tr>
		</table>
	</footer>	
</body>
</html>