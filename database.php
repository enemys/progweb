<?php
function connect_database() {
	//konfigurasi database
	$db_user = "gn15c6";
	$db_password = "GnVBE6CbFgL2";
	$db_name = "gn15c6";
	$db_host = "localhost";
	
	//akan selalu dilakukan, lebih baik jika ditulis dalam sebuah fungsi tersendiri
	$koneksi = mysqli_connect($db_host, $db_user, $db_password, $db_name);
	return $koneksi;
}

function get_all_user() {
	$koneksi = connect_database();
	$sql = "SELECT Username, Email FROM user";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$user_array = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$single_user = array("username" => $row['Username'], "Email" => $row['Email']);
		$user_array[] = $single_user;
	}

	mysqli_close($koneksi);
	return $user_array;	
}

function get_user($username) {
	$koneksi = connect_database();
	$sql = "SELECT Username, Email, Fullname, Profilpicture FROM user where Username='$username' ";

	$result = mysqli_query($koneksi,$sql);
	//ubah ke bentuk array
	
	$result = $koneksi->query($sql);
	$row = mysqli_fetch_assoc($result);

	
	mysqli_close($koneksi);
	return $row;	
}

function update_user($nama,$email,$username)
{
	$koneksi = connect_database();
	$sql = "UPDATE user SET Email='$email', Fullname='$nama'  where Username='$username' ";

	$result = mysqli_query($koneksi,$sql);
	
	mysqli_close($koneksi);
}

function add_pp($image,$username)
{
	$koneksi= connect_database();

	$sql = "UPDATE user Set Profilpicture='$image' where Username='$username'";

	$result = mysqli_query($koneksi,$sql);

	mysqli_close($koneksi);
}


function add_user($username, $email, $fullname, $password) {
	$koneksi = connect_database();

	if($koneksi)
	{
		echo("connected");	
	}

	//escape input
	$username = mysqli_real_escape_string($koneksi, $username);
	$password = mysqli_real_escape_string($koneksi, $password);
	$email = mysqli_real_escape_string($koneksi, $email);
	$fullname = mysqli_real_escape_string($koneksi, $fullname);	

	$sql = "INSERT INTO user(Username, Fullname, Email, Password) VALUES (?, ?, ?, ?)";
	$stmt = mysqli_prepare($koneksi, $sql);
	if ( !$stmt ) {
	  die('mysqli error: '.mysqli_error($koneksi));
	}
	mysqli_stmt_bind_param($stmt,"ssss", $username, $fullname, $email, $password);
	mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $newid;
}



function login ($username, $password)
{
	$koneksi = connect_database();

	$username = mysqli_real_escape_string($koneksi, $username);
	$password = mysqli_real_escape_string($koneksi, $password);


	$sql = "Select * from user where Username = '$username' ";
	$result = mysqli_query($koneksi, $sql);
	if($result)
	{
		echo "Query success <br>";
	}

	$result = $koneksi->query($sql);
	$row = mysqli_fetch_assoc($result);

	$x = $row["Password"];


	echo"$password $username $x";

	
	if($password == $row["Password"])
	{
		mysqli_close($koneksi);
		return true;
	}
	else
	{
		mysqli_close($koneksi);
		return false;
	}
}

function delete_user($username) {
	$koneksi = connect_database();
	$sql = "DELETE FROM user WHERE Username = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	echo "success";
	
}

function add_post($username, $posting, $postid, $image, $kategori, $judul) {
	$koneksi = connect_database();

	if($koneksi)
	{
		echo("connected");	
	}

	//escape input
	$judul = mysqli_real_escape_string($koneksi, $judul);
	$posting = mysqli_real_escape_string($koneksi, $posting);
	$date = Date("d/m/Y");

	$sql = "INSERT INTO post(Username, Posting, Tanggal, PostID, Image, KategoriID, Judul) VALUES (?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?, ?)";
	$stmt = mysqli_prepare($koneksi, $sql);
	if ( !$stmt ) {
	  die('mysqli error: '.mysqli_error($koneksi));
	}
	mysqli_stmt_bind_param($stmt,"sssssis", $username, $posting, $date, $postid, $image, $kategori,$judul);
	$check=mysqli_stmt_execute($stmt);
	if($check)
	{
		echo "success";
	}
	else
	{
		 echo mysqli_error($koneksi);# code...
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function update_post($username, $posting, $image, $id, $kategori, $judul) {
	$koneksi = connect_database();

	if($koneksi)
	{
		echo("connected");	
	}

	//escape input
	$judul = mysqli_real_escape_string($koneksi, $judul);
	$posting = mysqli_real_escape_string($koneksi, $posting);

	

	$sql = "UPDATE post Set Username='$username', Posting='$posting', Image='$image', KategoriID='$kategori', Judul='$judul' where PostID='$id' ";
	
	$check=mysqli_query($koneksi,$sql);
	if($check)
	{
		echo "success";
	}
	else
	{
		 echo mysqli_error($koneksi);# code...
	}
	
	mysqli_close($koneksi);
}

function make_postid ()
{
	$koneksi = connect_database();

	$count= mysqli_query($koneksi,"SELECT PostID FROM post ORDER BY PostID DESC LIMIT 1");
	$row= mysqli_fetch_array($count);
	mysqli_close($koneksi);
	return $row[0];
}

function get_all_user_post($username) {
	$koneksi = connect_database();
	$sql = "SELECT Judul, Posting FROM post where Username='$username' ";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$user_array = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$single_user = array("Judul" => $row['Judul'], "Posting" => $row['Posting']);
		$user_array[] = $single_user;
	}

	mysqli_close($koneksi);
	return $user_array;	
}

function get_user_post($id) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM post where PostID= '$id' ";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$result = $koneksi->query($sql);
	$row = mysqli_fetch_assoc($result);


	mysqli_close($koneksi);
	return $row;	
}

function nl2br_v2($string ) 
{ 
   //remove carriage returns 
	$string = str_replace('\r\n', "<p>&nbsp&nbsp&nbsp&nbsp&nbsp", $string); 
	$string = str_replace('\\r\\n', "<p>&nbsp&nbsp&nbsp&nbsp&nbsp", $string); 
	$string = str_replace('\r', '', $string); 
	$string = str_replace('\t', '	', $string);

   //convert indent to whitespaces if it is a integer. 
    

   //replace newlines with "<br />\n$indent" 
	$string = str_replace('\n', "<br>", $string); 
   //add  the indent to the first line too 
 

   return $string; 
}  