<?php session_start(); 
if(isset($_SESSION["nama"]) )
{
	$var2 = "";
	$var = $_SESSION["nama"];
	if(isset($_GET["profile"]) )
	{
		if ($_GET["profile"]!= $var) {
			$var2=$var;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<script type="text/javascript" src="js/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>

	<title>UKDW Showcase home</title>
</head>
<body>
<header>
		<nav class="nav-main">
			<?php if(isset($_SESSION["nama"])):?>
			<ul>
				<?php if($_SESSION["nama"] == "Admin"): ?>
					<li>
						<a href="admin.php" class="nav-item sign">Admin</a>
					</li>
				<?php endif;?>
				<li >
					<a href="profile.php" class="nav-item sign">Profile , <?php echo $_SESSION["nama"]; ?></a>
				</li>
				<li>
					<a href="logout.php" class="nav-item sign">Sign Out</a>
				</li>
			</ul>
			<?php
			else: ?>
			<ul>
				<li >
					<a href="signup.html" class="nav-item sign">Sign up</a>
				</li>
				<li>
					<a href="login.html" class="nav-item sign">Sign in</a>
				</li>
			</ul>
		<?php  endif;?>
		</nav>
		<img src="images/FTI_showcase_header.png" alt="tampilan logo header">
	</header>
	<div id="navline">
		<nav class="nav-main" id="kirii">
			<ul>
				<li>
					<a href="index.php" class="nav-item">Home</a>
				</li>
				<li>
					<a href="categories.php" class="nav-item">Categories</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="category.php"> Akademik</a>
									</li>
									<li>
										<a href="category.php"> Non-Akademik</a>
									</li>
									<li>
										<a href="category.php"> Reguler</a>
									</li>
								</ul>
							</div>
						</div>
				</li>
				<li>
					<a href="reward.php" class="nav-item">Award</a>
						<div class="nav-content">
							<div class="nav-sub">
								<ul>
									<li>
										<a href="national.php"> National</a>
									</li>
									<li>
										<a href="reward.php"> International</a>
									</li>
									<li>
										<a href="reward.php"> Daerah </a>
									</li>
								</ul>
							</div>
						</div>
				</li>								
			</ul>
		</nav>
		<nav class="nav-main" id="kanann">				
			<ul>
				<?php if(isset($_SESSION["nama"])): ?>
				<li>
					<a href="add.php" class="nav-item">
						Add
					</a>
				</li>

				<?php endif;?>
				<li> 
					<div>
						<form method="get" action="search.php">					
							<input type="text" name="search" id="search-input" placeholder="Search">
							<input src="images/search-icon.png" onclick="validasiSearch()" name="submit" type="image" id="search-input-img" alt="logo search"/>
						</form>
					</div>
				</li>

							
			</ul>
		</nav>
	

	</div>

	<table>
		<tr>
			<td style="vertical-align: top; width: 25%; ">
				<div style="margin-top: 20px ">
					<?php 
						require_once("database.php");

						$koneksi= connect_database();
						if(isset($_GET["profile"]) )
						{
							if ($_GET["profile"]!= $var) {
								$var=$_GET["profile"];
							}
						}
						$row=get_user($var);
					?>
					<div>
						<?php 
						if ($row["Profilpicture"] !="") {
							?>
							<img src="<?php echo $row["Profilpicture"]?>" id="profilpicture" class="profil">
							<?php
						}
						else
						{?>
							<img src="#" id="profilpicture" class="profil">
						<?php
						}
						?>
						<br>
						<?php
							if($var == $_SESSION["nama"])
							{
							?>
								<form class="profil" action="addpp.php" method="post" enctype="multipart/form-data" >
									<input type="file" name="pp" id="pp" ></input>
									<input type="submit" value="Upload"></input>
								</form>
							<?php
							}
						?>

						
					</div>


					<div class="profil">
						<Form action="changeprofile.php" method="post" class="profil" >
								<h3><b>Profile</b></h3>
								<p>
									Username: 
									<br>
									<input type="text" class="inputtext" name="username" id="username" value=<?php echo $row["Username"]?> disabled>
								</p>
								
								<p>
									Full Name: 
									<br>
									<input type="text" class="inputtext" name="fullname" id="fullname" value=<?php echo $row["Fullname"]?> <?php if($var != $_SESSION["nama"]) { echo "disabled"; } ?>>
								</p>
								
								<p>
									Email:
									<br>
									<input type="text" class="inputtext" name="email" id="email"  value=<?php echo $row["Email"]?> 	<?php if($var != $_SESSION["nama"]) { echo "disabled"; } ?>
								</p>

								<?php
									if($var == $_SESSION["nama"])
									{
									?>
										<p>
											<input type="Submit" name="submit" id="submit" value="Change Profile">
											<button><a href="changepassword.php" style="text-decoration: none; color:black; ">Change Password</a></button>
										</p>
									<?php
									}
								?>
								
							</Form>
					</div>

					

				</div>
			</td>

			<td style="text-align:center; width: 75%">
				<div >
					<?php

						if($var2 != $var)
						{
							$page=0;

							if(!isset($_GET['page']))
							{
								$result=mysqli_query($koneksi,"SELECT * from post WHERE Username='$var' and Status = 2  Limit 0,5");
								$r=mysqli_query($koneksi,"SELECT * from post WHERE Status = 2 ");
							}
							else
							{
								$page=$_GET["page"]; 
								$result=mysqli_query($koneksi,"SELECT * from post WHERE Username='$var' and Status = 2  Limit $page,5");
								$r=mysqli_query($koneksi,"SELECT * from post WHERE Status = 2 ");
							}
							
							$count=0;
							$jmlh=mysqli_num_rows($r);
							
						}
						else
						{
							$result=mysqli_query($koneksi,"select * from post where Username='$var' ");
						}
						$count=0;
						
						while($row=mysqli_fetch_assoc($result)){
						$post = $row["Posting"];
						?>
							<div >
								<hr>
								<h3><?php echo  $row['Judul']?></h3>

								<?php if($row["Image"]!= ""):?>
								<img src=<?php  echo "$row[Image]"?> style="width: 300px; height: 150px; ">
								<?php endif; ?>	

								<span>
									<?php echo nl2br_v2('\r\n'.$post);?>
								</span>

								<?php
									if($var == $_SESSION["nama"])
									{
									?>
										<br>
										<button><a href="edit.php?id=<?php echo $row["PostID"] ?>" style="text-decoration: none; color:black;"  >Edit</a></button>
										<button><a href="deletepost.php?postid=<?php echo $row["PostID"] ?>" style="text-decoration: none; color:black;"  >Delete</a></button>
									<?php
									}

									if($row["Status"] == 0)
									{

										echo "<br>Belum Di Acc";
									}

								?>

								<?php 
									 if( isset($_SESSION["nama"]))
										{
											if($row["Username"] == $_SESSION["nama"]):?>
												<span style="float: right;"><span><?php echo $row["Vote"];?> </span><span>Vote</span></span>
								<?php 		else:?>
												<button onclick="vote( <?php echo $row['PostID'].",".$count;?> )" style="float: right;"><span name="voteval" id="voteval" class="voteval<?php echo $count;?>"><?php echo $row["Vote"];?> </span><span>&nbsp</span>Vote</button>	
								<?php 		endif;
										}?>
								
								<hr>
							</div>
						<?php
							$count+=1;
						}
					?>
						

				</div>
			</td>
		</tr>
	</table>
	
	<div style="text-align: center">
		<?php
			if($jmlh>5 and $var2!=$var)
			{
				if($page>0)
				{
					echo "<a href='profile.php?profile=".$var."&page=".($page-5)."	 '>back</a>";
					echo "&nbsp;";
				}
				if(($page+5)<$jmlh)
				{
					echo "<a href='profile.php?profile=".$var."&page=".($page+5)." '>next</a>";
				}
			} 	
		?>
		<br>
	</div>

			
	<footer>
		<table>
			<tr>
				<td>
					<img src="images/FTI_showcase_footer.png" alt="tampilan logo footer"> <br><br>
					<a href="contectus.php">Contact Us</a>
					<br>
					<a href="aboutus.php">About Us</a>
				</td>				
				<td>
					<p><b>UNIVERSITAS KRISTEN DUTA WACANA</b><br>Jl. Dr. Wahidin Sudiro Husodo No. 5 - 25<br>Yogyakarta 55224<br>Telp. 0274 - 563929 Fax. 0274 - 513235<br>Email: humas@staff.ukdw.ac.id<br></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" >
					<h4> &copy; Mahasiswa TI 2016</h4>
				</td>
			</tr>
		</table>
	</footer>	
</body>
</html>
<?php 
	} 
else header("Location: index.php");
?>