<?php
	session_start();
	$username = $_POST['Email'];
	$password = $_POST['Password'];

	require_once("database.php");
	$condition = login($username,$password);
	
	if($condition)
	{
		$_SESSION['nama']= $username;
		$_SESSION['status']= "login";
		session_regenerate_id(true);
		header("Location: index.php ");
	}
	else
	{
		header("Location: login.html ");
	}