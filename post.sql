-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2016 at 06:13 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gn15c6`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `Status` tinyint(1) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Posting` varchar(2000) NOT NULL,
  `Username` varchar(32) NOT NULL,
  `Tanggal` date NOT NULL,
  `PostID` int(12) NOT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `KategoriID` int(11) NOT NULL,
  `Vote` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`Status`, `Judul`, `Posting`, `Username`, `Tanggal`, `PostID`, `Image`, `KategoriID`, `Vote`) VALUES
(2, 'UKDW Showcase', 'Website ini dibuat untuk mewadahi mahasiswa FTI UKDW dalam menampilkan hasil karya mereka kepada khalayak umum, baik seluruh mahasiswa UKDW maupun bukan mahasiswa UKDW. Seluruh mahasiswa FTI UKDW dapat ikut berpartisipasi dalam website ini. Mereka dapat membuat akun khusus dengan email @ti.ukdw.ac.id agar dapat menjadi narasumber pada wbsite ini. Tentunya untuk mengawasi isi content yang ada, akan ada satu user yang bertindak sebagai admin, dimana tugas admin adalah memfilter content yang di upload oleh narasumber lalu memberikan persetujuan apakah content dari narasumber layak untuk dipublikasikan maupun tidak. Jika tidak admin berhak untuk tidak mempublikasikan content dari narasumber dan dapat memberikan warning.\\r\\n				Harapan dengan adanya website ini seluruh khalayak umum dapat melihat secara keseluruhan kegiatan-kegiatan yang dilakukan oleh mahasiswa FTI UKDW secara umum, baik dari segi prestasi maupun kegiatan reguler yang dilakukan dengan berita-berita terbaru.\\r\\n', 'Admin', '2016-04-24', 1, 'post/Admin26-04-2016-01-36-05.jpg', 6, 0),
(2, 'Visi & Misi HMTI UKDW', 'Visi:\\r\\nMenjadikan Teknik Informatika yang Aktif dan Produktif dalam bidang Akademik dan non-Akademik\\r\\nMisi: \\r\\nMenyelenggarakan kegiatan yang mengembangkan mahasiswa dalam bidang akademik maupun non-akademik \\r\\nMemacu menekankan rasa kepedulian mahasiswa Teknik Informatika terhadap lingkungan kampus \\r\\nMemacu dan mengembangkan potensi mahasiswa untuk berorganisasi, berprestasi dan memajukan Teknik Informatika UKDW \\r\\nMenjadi wadah Aspirasi bagi civitas Teknik Informatika UKDW.', 'Admin', '2016-04-25', 2, 'post/Admin25-04-2016-02-13-36.jpg', 2, 0),
(2, 'asdasd', 'assda', 'Admin', '2016-04-26', 4, '', 4, 0),
(2, 'asdas', 'asdas', 'Admin', '2016-04-26', 5, '', 2, 0),
(2, 'asdafaasd', 'sdasdag', 'Admin', '2016-04-26', 6, '', 3, 0),
(2, 'asdaf', 'a\\r\\na\\r\\na\\r\\na\\r\\na\\r\\na\\r\\na\\r\\naa\\r\\na\\r\\n', 'Admin', '2016-04-26', 7, '', 3, 0),
(2, 'Mencoba saja', 'hanya mencoba', 'Admin', '2016-05-06', 8, '', 6, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`PostID`),
  ADD KEY `Username` (`Username`),
  ADD KEY `KategoriID` (`KategoriID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `user` (`Username`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`KategoriID`) REFERENCES `kategori` (`Kode`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
